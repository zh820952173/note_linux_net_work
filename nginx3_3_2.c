#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <signal.h> 

//信号处理函
void sig_usr(int signo)
{
    //这里用malloc，真是错用，不可重入函数不能在信号处理函数中  
    int *p;
    p = (int *)malloc (sizeof(int));
    free(p);
    if (signo == SIGUSR1)
    {
        printf("收到了信号SIGUSR1！\n");
    }
    else if (signo == SIGUSR2)
    {
        printf("收到了信号SIGUSR2！\n");
    }
    else
    {
        printf("收到了未捕捉到的信号！\n");
    }
}

int main(int argc, char **argv)
{
    if (signal(SIGUSR1, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉SIG_USR1信号！\n");
    }
    if (signal(SIGUSR2, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉到SIGUSR2信号！\n");
    }
    for (;;)
    {
        int *p;
        p = (int *)malloc(sizeof(int));
        free(p);
    }
    return 0;
}