## 在配置文件中增加work_connection项
## epoll函数实战
* epoll_create(),epoll_ctl(),epoll_wait();系统提供的函数调用
* ngx_epoll_init()函数的内容
    * epoll_create()：创建一个epoll对象，创建一个红黑树，还创建一个双向链表
    * 连接池：数组，元素数量就是work_connection【1024】，每个数组元素类型为ngx_connection_t【结构】【结构数组】
        * 为什么引入这个数组：2个监听套接字，用户连入进来，每个用户多出来一个套接字，把套接字数字和一块内存捆绑，达到的效果就是我将来通过这个套接字，就能把这块内存拿出来
* ngx_get_connection()重要函数：从连接池中找空闲连接
    * epoll_create()
    * 连接池（找空闲连接）
    * ngx_epoll_add_event()
        * epoll_ctl()
    * ev.data.ptr = (void *)((uintptr_t)c | c -> instance); 把一个指针和一个位合二为一，赛到一个void *中去，后续能够把这两个值全部取出来
* 总结：
    * epoll_create(),epoll_crl()
    * 连接池技巧,ngx_get_connection(),ngx_free_connection()
    * 同时传递一个指针和一个二进制数字技巧