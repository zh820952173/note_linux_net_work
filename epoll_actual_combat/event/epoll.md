## ngx_epoll_process_events函数的调用位置（等待接收和处理事件,开始获取发生的事件信息）
* epoll_create,epoll_ctl；--我们目前已经做好准备，等待迎接客户端主动发起三次握手连入
* 调用位置
    ```
    ngx_master_process_cycle()        //创建子进程等一系列动作
        ngx_setproctitle()            //设置进程标题  
        ngx_start_worker_processes()  //创建worker子进程   
            for (i = 0; i < threadnums; i++)   //master进程在走这个循环，来创建若干个子进程
                ngx_spawn_process(i,"worker process");
                    pid = fork(); //分叉，从原来的一个master进程（一个叉），分成两个叉（原有的master进程，以及一个新fork()出来的worker进程,只有子进程这个分叉才会执行ngx_worker_process_cycle()
                    ngx_worker_process_cycle(inum,pprocname);  //子进程分叉
                        ngx_worker_process_init();
                            sigemptyset(&set);
                            sigprocmask(SIG_SETMASK, &set, NULL); //允许接收所有信号
                            g_socket.ngx_epoll_init();  //初始化epoll相关内容，同时 往监听socket上增加监听事件，从而开始让监听端口履行其职责
                                m_epollhandle = epoll_create(m_worker_connections);
                                ngx_epoll_add_event((*pos)->fd....);
                                    epoll_ctl(m_epollhandle,eventtype,fd,&ev);
                        ngx_setproctitle(pprocname);          //重新为子进程设置标题为worker process
                        for ( ;; ) 
                        {
                            //子进程开始在这里不断的死循环
                            ngx_process_events_and_timers(); //处理网络事件和定时器事件
                                g_socket.ngx_epoll_process_events(-1); //-1表示卡着等待吧
                        }
        sigemptyset(&set); 
        for ( ;; ) {}.                //父进程[master进程]会一直在这里循环
    ```
* 从ngx_epoll_process_events函数的调用位置，可以知道：
    * 这个函数仍旧在子进程中调用
    * 这个函数放在了子进程的for(;;),这意味着这个函数会被不断的调用

## ngx_epoll_process_events函数的内容
用户三次三次握手成功连入进来，这个连入进来这个事件对于我们服务器来说就是一个监听套接字上的可读事件
* 事件驱动：官方nginx本身的架构也被称为事件驱动架构
    * 就是通过获取事件，通过获取到的事件并根据这个事件来调用适当的函数从而让整个程序干活

## ngx_event_accept函数的内容
* accept4/accept //接受连接
* ngx_get_connection //从连接池中获取连接，是针对新连入用户的连接
* ngx_epoll_add_event //客户端主动发送第一次的数据，这里将读事件加入epoll监控

## epoll的两种工作模式：LT和ET
epoll对文件描述符的操作模式
* LT（level trigged）：水平触发（默认的工作模式）
    * 对于采用LT模式的文件描述符，当epoll_wait检测其上有事件发送并将此事件通知应用程序，应用程序可以不立即处理该事件，这样，当应用程序下一次调用epoll_wait时，epoll_wait还会再次向应用程序通知该事件，直至该事件被处理（来一个事件，如果不处理它，那么这个事件会被一直触发）
* ET（edge trigged）：边缘触发
    * 对于采用ET模式的文件描述符，当epoll_wait检测到其上有事件发生并将此事件通知应用程序后，应用程序必须立即处理该事件，因为后续的epoll_wait调用将不在向应用程序通知该事件（只对非阻塞socket有用，来一个事件，内核只会通知你一次【不管你是否处理，内核都不会再次通知你】）（因为只触发一次，必须保证把该处理的事件处理利索）
* ET模式在很大程度上降低了同一个epoll事件被重复触发的次数，效率明显比LT高（因为触发次数少，消耗资源少）
* 每个使用ET模式的文件描述符都应该是非阻塞的，如果文件描述符是阻塞的，那么读或者写操作将会因为后续的事件而一直处于阻塞状态（饥渴状态）
* 现状：所有的监听套接字用的都是LT模式（防止连接的缺失），连入的用户套接字都是ET模式

## 事件驱动总结
* 事件驱动框架/事件驱动架构
    * 由一些事件发生源，通过事件收集器来收集和分发事件进行处理
        * 事件发生源：三次握手的内核通知，事件发生源是客户端
        * 事件收集器：epoll_wait()
        * 事件处理器：ngx_event_accept(),ngx_wait_request_handler()

## 问题
* 使用linux epoll模型，LT模式，当socket可写时，会不停的触发socket可写事件，如何处理？
    * 普遍的方式：需要向socket写数据的时候才把socket加入epoll【红黑树】，等待可写事件。接受到可写事件后，调用write或者send发生数据，当所有的数据都写完后，把socket移除epoll，这种方式的缺点是，即使发送很少的数据，也要把socket加入epoll，写完后再移除epoll，有一定的操作代价
    * 改进的方式：开始不把socket加入epoll，需要向socket写数据的时候，直接调用write或者send发送数据，如果返回EAGAIN，把socket加入epoll，再epoll的驱动下写数据【，全部数据发送完毕后，再移除epoll
    ，这种方式的优点是再数据不多的时候可以避免epoll的事件处理，提高效率


        