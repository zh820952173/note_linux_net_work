# 内存泄漏检测工具valgrind

## 介绍  
valgrind是一套linux下开源的仿真调试工具的集合。valgrind由内核（core）以及基于内核的其他调试工具组成。内核类似于一个框架（framework），它模拟了一个cpu环境，并且提供服务给其他工具；而其他工具则类似于插件（plug-in），利用内核提供的服务完成各种特定的内存调试任务。

## 工具  
### memcheck
最常用的工具，用来检测程序中出现的内存问题，所有对内存的读写都会被检测到，一切对malloc/free/new/delete的调用都会被捕获到，所以它可以检测以下问题：（C++强大的一点在于能够利用指针自由地控制内存的使用，适时的申请内存和释放内存，做到其他编程语言做不到的高效运行）
> 1. 对未初始化内存的使用
> 
> 2. 读/写释放后的内存块
> 3. 读/写超出malloc分配的内存块
> 4. 读/写不适当的栈中内存块
> 5. 内存泄漏，指向一块内存的指针永远丢失
> 6. 不正确的malloc/free或者new/delete匹配
> 7. memcpy相关函数中的dst和src指针重叠

## 安装
* 通过命令安装
  ```
  sudo apt install valgrind
  sudo yum install valgrind
  ```
* 通过源码安装
  ```
  wget https://sourceware.org/pub/valgrind/valgrind-3.16.1.tar.bz2 #进入官网下载最新版本
  tar -zxvf valgrind-3.16.1.tar.bz2 #解压源码包
  cd valgrind-3.16.1 #进入解压文件中
  ./configure --prefix=/usr/local/valgrind #指定安装目录
  make
  make install
  ```

## 命令介绍
* --tool=toolname
* h -help 显示帮助信息
* q -quiet 安静的退出，只打印错误的信息
* v -verbose 更详细的信息，增加错误数统计
* -leak-check=no/summary/full 要求对leak给出详细信息？

## 应用
 > 1. 编译程序
  ```
  gcc -Wall -g 1.c -o 1 #Wall提示所有告警 -g gdb -o 命名
  ```
  > 2. 使用valgrind检查程序bug
  ```
  valgrind --tool=memcheck --leak-check=full --show-reachable ./1 #--leak-check=full 所有泄漏检查,--show-reachable 显示泄漏地点
  ```
* 数组越界/内存未释放(1.c)
* 内存释放后读写（2.c）
* 无效读写（3.c）
* 内存泄漏（4.c）
* 内存多次释放(5.c)
* 内存动态管理，常见的内存分配方式分三种：静态存储，栈上分配，堆上分配。全局变量属于静态存储，他们在编译的时候就分配了存储空间；函数内的局部变量属于栈上分配；而最灵活的内存使用方式当属堆上分配，也叫内存动态分配，常见的内存动态分配函数包括：malloc,alloc,realloc,new，动态释放函数包括：free，delete。一旦成功申请了动态内存，我们就需要对其进行内存管理，而这又是最容易犯错误的。（6.c）

## 错误信息详解
> 1. Invalid read of size 4:非法读取/写入错误
> 2. LEAK SUMMARY：内存泄漏检查
```
definitely lost: 4 bytes in 1 blocks：绝对丢失，这种情况应该由程序员来解决，下面几种情况，可以当作参考
indirectly lost: 0 bytes in 0 blocks：间接丢失
possibly lost: 0 bytes in 0 blocks：可能丢失
still reachable: 0 bytes in 0 blocks：仍然可以访问
suppressed: 0 bytes in 0 blocks：抑制错误中的丢失
```
> 3. Invalid free() / delete / delete[] / realloc():非法释放
> 4. Conditional jump or move depends on uninitialised value(s):使用未初始化的变量

## 查看内存泄漏的三个地方（申请未释放的情况）
* 1. HEAP SUMMARY：total heap usage: 10 allocs, 7 frees, 81,785 bytes allocated，差值是1就没有泄漏，超过1就是有泄漏
* 2. 中间诸如： by 0x401672: CConfig::Load(char const*) (ngx_c_conf.cpp:77)和我们自己源代码有关的提示，就要主要
* 3. LEAK SUMMARY:definitely lost: 1,100 bytes in 2 blocks


 
