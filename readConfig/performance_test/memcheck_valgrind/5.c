#include <stdio.h>
#include <stdlib.h>

int main()
{
    char *p;
    p = (char *)malloc(sizeof(char) * 100);
    if(p)
    {
        printf("memory allocated at: %s\n", p);
    }
    else
    {
        printf("not enough memory!\n");
    }
    free(p);
    free(p);
    return 0;
}