#include <stdlib.h>
#include <stdio.h>

int main()
{
    char *p = malloc(1); //分配
    *p = 'a';
    char c = *p;
    printf("\n %c \n", c);
    free(p); //释放
    c =*p; //取值
    return 0;
}