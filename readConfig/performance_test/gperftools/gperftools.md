# 使用gperftools对程序进行性能分析
gperftools是google出品的一个性能分析工具，具体见https://github.com/gperftools/gperftools/wiki。
gperftools性能分析通过抽样方法完成，默认是一秒100个样本，即一个样本是10毫秒，因此程序运行时间要长一些

# 安装gperftools
* 安装automake
  ```
  sudo apt install automake
  ```

* 编译安装libunwind（如果是64位系统）
  ```
  wget https://github.com/libunwind/libunwind/releases/download/v1.5/libunwind-1.5.0.tar.gz # 从https://github.com/libunwind/libunwind/releases下载最新版本的libunwind源码包
  cd 解压源码目录
  ./configure
  make 
  make install
 
  ```

* 编译安装gperftools
  ```
  https://github.com/gperftools/gperftools/releases/download/gperftools-2.9.1/gperftools-2.9.1.tar.gz # 从https://github.com/gperftools/gperftools/releases下载最新版本的gperftools源码包
  cd 解压源码目录
  ./autogen.sh
  ./configure
  make
  make install

  ```

# 使用
* 运行一段时间就会正常退出的程序的性能分析  
  > 这种情况我们可以直接在代码中插入性能分析函数（案例1.c）  

  > 编译运行，注意编译时需要链接tcmalloc和profiler库  
  ```
  gcc -o 1 1.c -ltcmalloc -lprofiler
  ```
  
  > 运行后会生成test.prof文件，然后用pprof命令就可以生成text的分析报告
  ```
  pprof --text 1 test.prof
  ```
