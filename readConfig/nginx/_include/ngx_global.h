#ifndef __NGX_GLOBAL_H__
#define __NGX_GLOBAL_H__

//一些比较通用的定义放在这里

//结构定义
typedef struct
{
    char ItemName[50];
    char ItemContent[500];
}CConfItem, *LPCConfItem; //CConfItem是结构，*LPCConfItem是指向结构的指针

//外部全局变量声明

extern char **g_os_argv;
extern char *gp_envmem;
extern int g_environlen;

#endif