#include <stdio.h>
#include <unistd.h>

// #include <signal.h>

int main(int argc, char *const *argv)
{
    pid_t pid;
    printf("hello,nginx\n";

    //系统函数，设置某个信号来的时候处理程序（用哪个函数处理）
    // signal(SIGHUP,SIG_IGN);//SIG_IGN标志：我要求忽略这个信号，请操作系统不要用缺省的处理方式来对待我
    
    pid =fork();//系统函数，用来创建新的进程，子进程会从fork（）调用之后开始执行
    if(pid < 0)
    {
        printf("fork()进程出错\n");
    }
    else if(pid == 0)
    {
        //子进程这个条件满足
        printf("子进程开始执行\n");
        setsid();//新建立一个不同的session，但是进程组组长调用setsid（）是无效的
        for(;;) 
        {
            sleep(1);//休息1秒
            printf("子进程休息1秒\n");
        }

    }
    else
    {
        //父进程会走这里
        for(;;) 
        {
            sleep(1);//休息1秒
            printf("父进程休息1秒\n");
        }
    }
    /*for(;;) 
    {
        sleep(1);//休息1秒
        printf("休息1秒\n");
    }*/
    printf("程序退出，再见\n");
    return 0;
}

