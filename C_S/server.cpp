
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>

#define SERV_PORT 9000  //本服务器要监听的端口号，一般1024以下的端口很多都是属于周知端口，所以我们一般采用1024之后的数字做端口号

int main(int argc, char *const *argv)
{
    //这些演示代码的写法都是固定套路，一般都这么写

    //服务器的socket套接字[文件描述符]
    int listenfd = socket(AF_INET, SOCK_STREAM, 0); //创建服务器的socket

    struct sockaddr_in serv_addr;   //服务器的socket地址结构体
    memset(&serv_addr, 0, sizeof(serv_addr));   //清空serv_addr

    //设置本服务器要监听的地址和端口，这样客户端才能连接到该地址和端口并且发送数据
    serv_addr.sin_family = AF_INET;     //选择协议族为ipv4
    serv_addr.sin_port = htons(SERV_PORT);  //绑定我们自定义的端口，客户端程序和我们服务器程序通讯时，就要往这个端口连接和传送数据
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);  //监听本地所有的ip地址，INADDR_ANY表示的是一个服务器上所有的网卡（服务器上不止一个网卡），多个本地ip地址都进行绑定端口号，进行侦听

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)); //绑定服务器地址结构体
    listen(listenfd, 32); //参数2表示服务器可以积压未处理完的连入请求总数，客户端来一个未连入的请求，请求数+1，连入请求完成，c/s之间进入正常通讯后，请求数-1
    
    int connfd;
    const char *pcontent = "I sent sth to client!"; //指向常量字符区的指针
    for(;;)
    {
        //卡在这里，等客户端连接，客户端连入后，该函数走下去（注意这里返回的是一个新的socket—connfd，后续本服务器就用connfd和客户端之间收发数据，而原有的listenfd依旧用于继续监听其他连接）
        connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);

        //发送数据给客户端
        write(connfd, pcontent, strlen(pcontent)); //注意第一个参数是accept返回的connfd套接字

        //只给客户端发送一个信息，然后直接关闭套接字连接
        close(connfd);
    }
    close(listenfd); //实际本简单范例走不到这里，这句暂时看起来没啥用
    return 0;
}