* **IO复用使得程序能够同时监听多个文件描述符** 

## epoll技术简介
* I/O多路复用：epoll就是一种典型的I/O多路复用技术：epoll技术的最大特点是支持高并发；传统的I/O多路复用技术select/poll，在并发量达到1000-2000的时候，性能就会下降；epoll从linux内核2.6引入的，2.6之前是没有的
* epoll和kqueue技术类似：单独一台计算机支撑少则数万，多则数十上百万并发连接的核心技术；epoll技术完全没有这种性能会随着并发量提高而出现明显下降的问题，但是并发量每增加一个，必定要消耗一定的内存区保存这个连接相关的数据，并发量总还是有限的，不可能是无限的
* 10万个连接同一时刻，可能只有几十个上百个客户端给你发送数据，epoll只处理这几十上百个客户端
* 很多服务器程序在用多进程，每个进程对应一个连接；也有用多线程的，每个线程对应一个连接；epoll事件驱动机制，在单独的进程或者线程里运行，收集/处理事件；没有进程/线程之间切换的消耗，所以比较高效

## epoll原理与函数介绍：三个函数，理解好就等于掌握了epoll技术的工作原理
可以参考 https://githup.com/wangbojing 仓库中的ntytcp项目与 c1000k_test（测试百万并发的测试程序）
在项目ntytcp中主要看：nty_epoll_inner.h nty_epoll_rb.c
```
epoll_create()
epoll_ctl()
epoll_wait()
epoll_event_callback() //回调函数
```
学完实战代码，在去看项目中的epoll源码，事半功倍
* epoll_create()函数
    ```
    int epoll_create(int size)
    ```
    * 功能：创建一个epoll对象，返回该对象的描述符【文件描述符】，这个描述符就代表则会个epoll对象(底层是创建一个空的红黑树和一个空的双向链表)，后续的两个函数的第一个参数都是这个 ，注意这个epoll对象最终需要被close掉，因为文件描述符/句柄总是关闭的，这里的size > 0 即可
    * 原理：
        * 1. struct eventpoll \*ep = (struct eventpoll\*)calloc(1, sizeof(struct eventpoll)); //相当于new了一个eventpoll对象【开辟了一块内存】
        * 2. rbr结构成员：代表一个红黑树的根节点【刚开始指向空】，把rbr理解为红黑树的根节点的指针；红黑树用来保存键【数字】值【结构】对，能够快速的通过你给的key，把整个的键值取出来
        * 3. rdlist结构成员：代表一个双向链表的表头指针；双向链表：从头访问/遍历每个元素特别快；next
        * 4. 总结：创建了一个eventpoll对象，被系统保存起来；rbr成员被初始化成指向一个空红黑树的根【有了一个红黑树】；rdlist成员初始化成指向一个空双向链表【有了一个双向链表】
* epoll_ctl()函数
    ```
    int epoll_ctl(int epfd, int op, int sockfd, struct epoll_event *event)
    ```
    * 功能：把一个socket以及这个socket相关的事件添加到这个epoll对象描述符上去，目的就是通过这个epoll对象来监视这个socket【客户端的tcp连接】上的数据来往情况；当有数据来往的时候，系统会通知我们；我们把感兴趣的事件通过epoll_ctl()添加到系统上，当这个事件来的时候，系统会通知我们
    * epfd：epoll_create()返回的epoll对象描述符
    * sockfd：表示客户端连接，就是你从accept()返回的连接socket：这个是红黑树里面的key
    * op：操作类型，动作，对应数字1，2，3，EPOLL_CTL_ADD,EPOLL_CTL_DEL,EPOLL_CTL_MOD
        * EPOLL_CTL_ADD添加事件：等于往红黑树中增加一个节点，每个客户端连入服务器后，服务器都会产生一个对应的socket，每个连接这个socket都不重复，所以这个socket就是红黑树中的key，把这个节点添加到红黑树中去
        * EPOLL_CTL_MOD：修改事件，用了EPOLL_CTL_ADD把节点添加到红黑树之后，才存在修改
        * EPOLL_CTL_DEL：从红黑树上把这个节点干掉，这会导致这个socket上【tcp连接上】无法收到任何系统通知事件
    * event：事件信息，这里包括的是一些事件信息，
    * 原理：
        * epi = (struct epitem*)calloc(1, sizeof(struct epitem)); //生成了一个epitem对象，大家注意这个结构epitem，这个结构对象，其实就是红黑的一个节点，也就是说，红黑树的每个节点都是 一个epitem对象；
        * epi = RB_INSERT(_epoll_rb_socket, &ep->rbr, epi); //增加节点到红黑树上
        * epitem.rbn,代表三个指针，分别指向红黑树的左子树，右子树，父亲
        * epi = RB_REMOVE(_epoll_rb_socket, &ep->rbr, epi); //从红黑树上把这个节点干掉
        * EPOLL_CTL_MOD：在红黑树上找到这个节点，然后修改这个节点的内容
    * 总结：
        * EPOLL_CTL_ADD：等价于往红黑树上加节点  
        * EPOLL_CTL_DEL：等价于从红黑树上删除节点  
        * EPOLL_CTL_MOD：等价于修改已有红黑树的节点  
        
* epoll_wait()函数：当事件发送，我们如何拿到操作系统的通知
    ```
    int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeouts)
    ```
    * 功能：阻塞一小段时间并等待事件发生，返回事件集合，也就是获取内核的事件通知，说白了就是遍历这个双向链表，把这个双向链表里边的节点数据拷贝出去，拷贝完毕的就从双向链表里移除
    * 双向链表里面记录的所有有数据/有事件的socket【tcp连接】
    * 参数：
        * epfd：epoll对象描述符
        * events：是内存，也是数组长度是maxevents，表示此次epoll_wait调用可以收集到的maxevents个已经继续的读写事件【已经准备好的读写事件】，说白了就是返回的实际发生事件的tcp连接的数目*
        * timeouts:阻塞等待的时长
    * epitem结构设计的高明之处在于：既能够作为红黑树的节点，也能够作为双向链表的节点
    * 总结：epoll_wait函数如果检测到事件，就讲所有就绪的事件从内核事件表【由epfd参数指定】中复制到到第二个参数events指向的数组中，这个数组只用于输出epoll_wait检测到的就绪事件

* 内核双向链表增加节点
    一般有四种情况，会使操作系统把节点插入到双向链表中：
    * 客户端完成三次握手，服务器要accept()
    * 当客户端关闭连接，服务器也要调用close()关闭
    * 客户端发数据来的，服务器要调用read(),recv()函数来收数据
    * 当可以发送数据时，服务器可以调用send(),write()
    * 其他情况在实战代码中再说

**select函数：**  
>用途：在一段时间内，监听用户感兴趣的文件描述符上的可读、可写、异常的时间；  

>select函数和之前的recv、send函数直接操作文件描述符不同，使用select函数可以先对需要操作的文件描述符进行查询，查看目标文件描述符是否可以可读、可写、异常操作。  

>select成功时返回就绪（可读、可写、异常）文件描述符的总数。如果在超时时间内，没有任何文件描述符就绪，select将返回0.select失败时返回-1并设置errno。如果select等待期间，程序收到信号，则select立即返回-1，并设置errno为EINTR。  

**epoll：**  
> epoll函数是linux特有的IO复用函数，其实现和使用于select、poll有极大区别；  
 
> epoll使用一组函数来完成任务，而不是单个任务；     

> epoll把用户关心的文件描述符上的事件放在内核里的一个事件表上，从而无须像select、poll那样每次调用都要重复传入文件描述符集或者事件集；    

> 但是epoll需要使用一个额外的文件描述符，来唯一标识内核中的这个事件表；  

>>epoll_create函数:创建一个额外的文件描述符，来唯一标识内核中的这个事件表；该函数的返回文件描述符将用作其他所有epoll系统调用的第一个参数，以指定要访问的内核事件表；

>>epoll_ctl:操作epoll内核事件表的函数，成功返回0，失败返回-1并设置errno；  

>>epoll_wait:在一段时间内等待一组文件描述符上的事件，成功时返回就绪的文件描述符的个数，失败时返回-1并设置errno，epoll_wait函数如果检测到事件，将所有就绪的事件从内核事件表中复制带它的第二个参数指向的数组中，这个数组只用于输出epoll_wait检测到的就绪事件，而不像select、poll的数组那样即用于传入用户注册的事件，又用于输出内核检测到的就绪事件。  

>epoll对文件描述符的操作有两种模式：LT和ET；LT是默认的工作模式，这种模式epoll相当于一个效率高的poll。当往epoll内核时间表注册一个文件描述符上的EPOLLET事件时，epoll将以ET模式来操作该文件描述符，ET时epoll高效的工作模式；  

>>LT模式：对于采用LT模式的文件描述符，当epoll_wait检测到其上有事件发生并将此事件通知应用程序后，应用程序可以不立即处理此事件，这样当应用程序下一次调用epoll_wait时，epoll_wait还会再次向应用程序通告此事件直到事件被处理；  

>>ET模式：对于采用ET模式的文件描述符，当epoll_wait检测到其上有事件发生并将此事件通知应用程序，应用程序必须立即处理此事件，因为后续的epoll_wait调用将不再向应用程序通知该事件。可见ET模式在很大程度降低了同一个epoll_wait事件被重复触发的次数，因此效率要比LT模式高。  


# EPOLLONESHOT事件  
> 即使使用ET模式，一个socket上的某个事件还是可能被触发多次，这在并发程序中会引起一个问题。比如一个线程（或者进程）在读取完某个socket上的数据后开始处理这些数据，而在处理的过程中该socket上又有新数据可读（EPOLLIN再次被触发），此时另外一个线程被唤醒来读取这些新的数据，于是出现了两个线程同时操作同一个socket的局面，不是我们所期望的，我们期望的是一个socket连接在同一时刻都只被同一个线程处理，这一点可以使用epoll的EPOLLONESHOT事件实现。  

> 对于注册了EPOLLONESHOT事件的文件描述符，操作系统最多触发其上注册的一个可读、可写、异常的事件，且只触发一次，除非使用epoll_ctl函数重置该文件描述符上注册的EPOLLONESHOT事件。这样当一个线程在处理socket时，其他线程是不可能有机会操作该socket的，反过来说，注册了EPOLLONESHOT事件的socket一旦被某个线程处理完毕，该线程就应该立即重置这个socket上的EPOLLONESHOT事件，以确保这个socket下一次可读时，其EPOLLONESGHOT事件能被触发，进而让其他工作线程有机会继续处理这个socket。 
