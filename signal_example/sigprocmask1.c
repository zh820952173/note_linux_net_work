#include <stdio.h>
#include <unistd.h>
#include <signal.h>
 void handler(int sig)
 {
     printf("Deal with SIGINT\n"); //SIGINT信号处理函数
 }
 
 int main(int argc, char **argv)
 {
     sigset_t newmask, oldmask, pendmask;

     struct sigaction act;
     act.sa_handler = handler; //handler未信号处理函数
     sigemptyset(&act.sa_mask);
     act.sa_flags = 0;
     sigaction(SIGINT, &act, 0); //信号捕捉函数，捕捉CTRL+c

     sigemptyset(&newmask); //初始化信号集
     sigaddset(&newmask, SIGINT); //将SIGINT信号添加到信号集  

     sigprocmask(SIG_BLOCK, &newmask, &oldmask); //将newmask中的SIGINT阻塞掉，并保存当前信号屏蔽字到oldmask

     //sleep(5); //休眠5秒，说明：在5s休眠时间内，任务SIGINT信号都会被阻塞，如果5s内收到任何键盘的ctrl+c信号，此时
                //会把这些信息存放在内核的队列中，等待5s结束后，可能要处理此信号
    int j =0;
    for (; j < 5; j ++)
    {
        sleep(1);
        printf("sleep %d s\n", j);
    }
     sigpending(&pendmask); //检查信号是悬而未决的，所谓悬而未决是指SINGINT被阻塞还没有被处理
     if (sigismember(&pendmask, SIGINT)) 
     {
         printf("SIGINT pending\n");
     }

     printf("SIGINT UNBLOCK\n");
     //sigprocmask(SIG_SETMASK, &oldmask, NULL); //恢复被屏蔽的信号SINGINT
     /* 
        此处开始处理信号，调用信号处理函数
    */
   int i = 0;
   while(1)
   {
       sleep(1);
       printf("sleeping %d s\n",i);
       i++;
   }
   return 0;
 }