#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void show_hander(int sig)
{
    printf("I got signal %d\n", sig);
    int i;
    for(i = 0; i < 5; i ++)
    {
        printf("i = %d\n", i);
        sleep(1);
    }
}

int main(int argc, char **argv)
{
    int i= 0;
    struct sigaction act, oldact;
    act.sa_handler = show_hander;
    sigaddset(&act.sa_mask, SIGQUIT); //如果在信号SIGINT（ctrl+c）的信号处理函数show_hander执行过程中，本进程收到信号SIGQUIT(ctrl+\)
                                      //将阻塞该信号，直到show_hander执行结束才会处理信号SIGQUIT
    //act.sa_flags = SA_RESETHAND | SA_NODEFER; //当调用信号处理函数时，将信号的处理函数重置为缺省值
    act.sa_flags = 0; //如果不需要重置该给定信号的处理函数为缺省值；并且不需要阻塞该给定信号(无须设置sa_flags标志)，那么必须将sa_flags清零
    sigaction(SIGINT, &act, &oldact);
    while (1)
    {
        sleep(1);
        printf("sleeping %d\n", i);
        i++;
    }
    return 0;
}