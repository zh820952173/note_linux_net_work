#include <stdio.h>
#include <stdlib.h> //malloc
#include <unistd.h>
#include <signal.h>

int main(int argc, char **argv)
{
    printf("进程开始执行！\n");
    write(STDOUT_FILENO, "aaabbb", 6);
    for (;;)
    {
        sleep(1); //休息1s
        //printf("休息1s，进程id = %d!\n", getpid());
    }
    printf("再见！\n");
    return 0;
}