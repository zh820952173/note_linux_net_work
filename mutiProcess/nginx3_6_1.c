#include <stdio.h> 
#include <unistd.h> 
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>

void sig_usr(int signo)
{
    printf("收到SIGUSR1信号，进程id =  %d\n", getpid());
}

int main(int argc, char **argv)
{
    pid_t pid;

    printf("进程开始执行!\n");

    //先简单创建一个信号
    if (signal(SIGUSR1, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉到SIGUSR1信号！\n");
        exit(1);
    }
 
    pid = fork(); //创建一个子进程

    //要判断子进程是否创建成功
    if (pid < 0)
    {
        printf("子进程创建失败，很遗憾！\n");
        exit(1);
    }

    //现在，父进程和子进程同时开始运行
    for (;;)
    {
        sleep(1); //休息1s
        printf("休息1s，进程id = %d\n", getpid());
    }
    printf("再见\n");
    return 0;
}