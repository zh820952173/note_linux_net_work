#include <stdio.h> 
#include <unistd.h> 
#include <signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/wait.h>

/*
    信号处理函数
*/
void sig_usr(int signo)
{
    int status;

    switch (signo)
    {
    case SIGUSR1:
        printf("收到了SIGUSR1信号，进程id = %d\n", getpid());
        break;
    
    case SIGCHLD:
        printf("收到了SIGCHLD信号，进程id = %d\n", getpid());
        //这里学了一个新函数waitpid，有时候也用wait，但只需要掌握使用waitpid即可 
        //这个waitpid说白了获取子进程的终止状态，这样子进程就不会变成僵尸进程了
        pid_t pid = waitpid(-1, &status, WNOHANG); //第一个参数为-1，表示等待任何一个子进程
                                                    //第二个参数保存子进程的状态信息
                                                    //第三个参数提供额外选项，WNOHANG表示补阻塞，让这个waitpid立即返回
    if (pid == 0)  //子进程没有结束，会立即返回这个数字，但这里应该不是这个数字
    {
        return;
    }
    if (pid == -1) //这表示这个waitpid调用有错误，有错误也理解返回出去，不管那么多
    {
        return;
    }
     
    //走到这里，表示成功，那也return吧
    return;
    break;
    }
}

int main(int argc, char **argv)
{
    pid_t pid;

    printf("进程开始执行!\n");

    //先简单创建一个信号
    if (signal(SIGUSR1, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉到SIGUSR1信号！\n");
        exit(1);
    }

    if (signal(SIGCHLD, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉SIGCHLD信号！\n");
        exit(1);
    }
 
    pid = fork(); //创建一个子进程

    //要判断子进程是否创建成功
    if (pid < 0)
    {
        printf("子进程创建失败，很遗憾！\n");
        exit(1);
    }

    //现在，父进程和子进程同时开始运行
    for (;;)
    {
        sleep(1); //休息1s
        printf("休息1s，进程id = %d\n", getpid());
    }
    printf("再见\n");
    return 0;
}