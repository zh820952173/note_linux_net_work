#include <stdio.h>
#include <unistd.h> //fork()
#include <sys/types.h> //pid_t
#include <stdlib.h> //malloc exit
#include <limits.h>

int main(int argc, char **argv)
{
    fork(); //一般fork都会成功，所以不判断返回值了，我们假定成功
    fork();
    
    //fork() &&fork() || (fork() && fork());
    printf("每个实际用户id的最大进程数 = %ld\n", sysconf(_SC_CHILD_MAX));

    for (;;)
    {
        sleep(1); //休息1s
        printf("休息1s，进程id = %d\n", getpid());
    }
    printf("再见了\n");
    return 0;
}