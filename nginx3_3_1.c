#include <stdio.h>
#include <stdlib.h> 
#include <unistd.h> 
#include <signal.h> 
#include <errno.h>

int g_mysign = 0;
void muNEfuc(int value) //我这个函数能够修改全局变量g_mysign的值
{
    //....其他处理代码
    g_mysign = value;
    //....其他处理代码
}

void sig_usr(int signo)
{
    //muNEfuc(22); //因为一些实际需求必须在sig_user这个信号处理函数里调用这个muNEfuc()

    int myerrno = errno;

    if (signo == SIGUSR1)
    {
        printf("收到了SIGUSR1信号！\n");
    }
    else if (signo == SIGUSR2)
    {
        printf("收到了SIGUSR2信号！\n");
    }
    else
    {
        printf("收到了未捕捉的信号%d！\n", signo);
    }

    errno = myerrno;
}

int main(int argc, char **argv)
{
    if (signal(SIGUSR1, sig_usr) == SIG_ERR) //系统函数，参数1：要捕获的信号类型；参数2：是个函数指针，用于指定前面信号参数的处理函数
    {
        printf("无法捕捉到SIGUSR1信号！\n");
    }
    if (signal(SIGUSR2, sig_usr) == SIG_ERR)
    {
        printf("无法捕捉到SIGUSR2信号！\n");
    }
    for (;;)
    {
        sleep(1);
        printf("休息1秒\n");

        muNEfuc(15);
        printf("g_mysign = :%d\n",g_mysign);
    }
    printf("再见！\n");
    return 0;
}