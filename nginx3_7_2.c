#include <stdio.h>
#include <stdlib.h> //malloc
#include <unistd.h>
#include <signal.h>

#include <sys/stat.h>
#include <fcntl.h>

//创建守护进程,创建成功返回1.否则返回-1
int ngx_dasemon()
{
    int fd;
    switch (fork()) //fork一个子进程
    {
    case -1:
        //创建子进程失败，这里可以写日志。。。。。。
        return -1;
    case 0:
        //子进程，走到这里，直接break；
        break;
    default:
        //父进程，直接退出
        exit(0);
    }

    //只有子进程流程才能走到这里
    if (setsid() == -1) //脱离终端，终端关闭，将跟此子进程无关
    {
        //记录错误日志
        return -1;
    }
    umask(0); //设置为0，不要让他来限制文件权限，以免引起混乱

    fd = open("/dev/null", O_RDWR); //打开黑洞设备，以读写方式打开
    if (fd == -1)
    {
        //记录错误日志
        return -1;
    }
    if (dup2(fd, STDIN_FILENO) == -1) //先关闭STDIN_FILENO（这是规矩,已经打开的描述符，动他之前，先close）
    {
        //记录错误日志。。。。。。
        return -1;
    }
    if (dup2(fd, STDOUT_FILENO) == -1) //先关闭STDIN_FILENO，类似于指针指向null，让/dev/null成为标准输出
    {
        //记录错误日志
        return -1;
    }
    if (fd > STDERR_FILENO) //fd应该是3，这个应该成立
    {
        if (close(fd) == -1) //释放资源这个文件描述符就可以被复用，不然这个数字（文件描述符）就会一直占着
        {
            //记录错误日志
            return -1;
        }
    }
    return 1;
}

int main(int argc, char **argv)
{
    if (ngx_dasemon() != 1)
    {
        //创建守护进程失败，可以做失败后的处理比如写日志等等
        return 1;
    }
    else
    {
        //创建守护进程成功，执行守护进程要干的活
        for (;;)
        {
            sleep(1); //休息1s
            printf("休息1s， 进程id = %d！\n", getpid()); //就算打印也没用，限制标准输出指向黑洞（/dev/null）,打印不出任何结果
        }
    }
    return  0;
}